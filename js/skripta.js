$(document).ready(function() {
  var baseUrl = "https://teaching.lavbic.net/OIS/chat/api";
  var user = {id: 54, name: "Jure Savnik"}; // TODO: vnesi svoje podatke
  var nextMessageId = 0;
  var currentRoom = "Skedenj";


  // Naloži seznam sob
  $.ajax({
    url: baseUrl + "/rooms",
    type: "GET",
    success: function (data) {
      for (i in data) {
        $("#rooms").append(" \
          <li class='media'> \
            <div class='media-body room' style='cursor: pointer;'> \
              <div class='media'> \
                <a class='pull-left' href='#'> \
                  <img class='media-object img-circle' src='img/" + data[i] + ".jpg' /> \
                </a> \
                <div class='media-body'> \
                  <h5>" + data[i] + "</h5> \
                </div> \
              </div> \
            </div> \
          </li>");
      }
    }
  });
  
    updateChat = function() {
    $.ajax({
      url: baseUrl + "/messages/" + currentRoom + "/" + nextMessageId,
      type: "GET",
      success: function (data) {
        for (i in data) {
          var message = data[i];
          $("#messages").append(" \
            <li class='media'> \
              <div class='media-body'> \
                <div class='media'> \
                  <a class='pull-left' href='#'> \
                    <img class='media-object img-circle' src='https://randomuser.me/api/portraits/men/" + message.user.id + ".jpg'> \
                  </a> \
                  <div class='media-body'> \
                    <small class='text-muted'>" + message.user.name+ " | " + message.time+ "</small> <br> \
                        " + message.text + " \
                    <hr> \
                  </div> \
                </div> \
              </div> \
            </li>");
          nextMessageId = Message.id+ 1;
        }
        setTimeout(function() {updateChat()} ,5000);
}
    });
  };

updateUsers = function() {
  $.ajax({
        url: baseUrl + "/users/" + currentRoom,
        type: "GET",
        success: function (data) {
          $("#users").html("");
          for (i in data) {
            var user = data[i];
            $("#users").append(" \
              <li class='media'> \
                <div class='media-body'> \
                  <div class='media'> \
                    <a class='pull-left' href='#'> \
                      <img class='media-object img-circle' src='https://randomuser.me/api/portraits/men/" + user.id + ".jpg' /> \
                    </a> \
                    <div class='media-body' > \
                      <h5>" + user.name+ "</h5> \
                    </div> \
                  </div> \
                </div> \
              </li>");
          }
          setTimeout(function() {updateUsers()} , 5000);
        }
      });
    };

  // Klic funkcije za začetek posodabljanja uporabnikov
  updateUsers();

  sendMessage = function () {
    $.ajax({
      url: baseUrl + "/messages/" + currentRoom,
      type: "POST",
      contentType: 'application/json',
      data: JSON.stringify({user: user, text: $("#message").val()}),
      success: function (data) {
        $("#message").val("");
      },
      error: function(err) {
        alert("Prišlo je do napake pri pošiljanju sporočila. Prosimo Odgovor poskusite znova!")
      }
    });
  };
  
  // On Click handler za pošiljanje sporočila
  $("#send").click(sendMessage);
  
  $.ajax({
    url: baseUrl + "/rooms",
    type: "GET",
    success: function (data) {
      for (i in data) {
        $("#rooms").append(" \
          <li class='media'> \
            <div class='media-body room' style='cursor: pointer;'> \
              <div class='media'> \
                <a class='pull-left' href='#'> \
                  <img class='media-object img-circle' src='img/" + data[i] + ".jpg'> \
                </a> \
                  <h5>" + data[i] + "</h5> \
                </div> \
              </div> \
            </div> \
          </li>");
      }
      $(".room").click(changeRoom);
    }
  });
  
    changeRoom = function (event) {
    $("#messages").html("");
    $("#users").html("");
currentRoom= event.currentTarget.getElementsByTagName("h5")[0].innerHTML;
nextMessageId = 0;
  };

});
